//
//  M3Globals.h
//  M3
//
//  Created by Wegile on 12/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M3Globals : NSObject

+ (id)shared;
- (NSString *)mimeTypeForData:(NSData *)data;
- (BOOL) validEmail:(NSString*) emailString;

@end
