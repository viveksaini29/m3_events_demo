//
//  M3Products.h
//  M3 Cart
//
//  Created by Wegile on 23/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M3Products : NSObject

@property(nonatomic,strong) NSString *productName;
@property(nonatomic,strong) NSString *productImage;
@property(nonatomic,strong) NSString *productPrice;
@property(nonatomic,strong) NSString *productDescription;
@property(nonatomic,strong) NSString *product_offeredPrice;
@property(nonatomic,strong) NSString *product_Id;

-(void)setProductInfo:(NSDictionary*)info;
@end
