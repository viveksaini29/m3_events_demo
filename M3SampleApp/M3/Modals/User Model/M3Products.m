//
//  M3Products.m
//  M3 Cart
//
//  Created by Wegile on 23/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3Products.h"
static M3Products *products;
@implementation M3Products

+ (id)shared {
    static M3Products *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)setProductInfo:(NSDictionary*)product_Info {
    
    if (product_Info.count > 0) {
        
        if (product_Info[@"productName"] != nil) {
            _productName = product_Info[@"productName"];
        }
        
        if (product_Info[@"productPrice"] != nil) {
            _productPrice = product_Info[@"productPrice"];
        }
        
        if (product_Info[@"productImage"] != nil) {
            _productImage = product_Info[@"productImage"];
        }
        
        if (product_Info[@"productDescription"] != nil) {
            _productDescription = product_Info[@"productDescription"];
        }
        
        if (product_Info[@"offeredPrice"] != nil) {
            _product_offeredPrice = product_Info[@"offeredPrice"];
        }
        
        if (product_Info[@"_id"] != nil) {
            _product_Id = product_Info[@"_id"];
        }
    }
}
@end
