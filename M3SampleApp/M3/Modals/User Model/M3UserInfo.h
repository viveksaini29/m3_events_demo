//
//  M3UserInfo.h
//  M3
//
//  Created by Wegile on 13/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M3UserInfo : NSObject
+ (id)shared;

@property(nonatomic,strong) NSString *user_FirstName;
@property(nonatomic,strong) NSString *user_LastName;
@property(nonatomic,strong) NSString *user_Image;
@property(nonatomic,strong) NSString *user_Id;
@property(nonatomic,strong) NSString *device_Id;
@property(nonatomic,strong) NSString *app_Id;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *user_Email;
@property(nonatomic,strong) NSString *user_ContactNumber;
@property(nonatomic,strong) NSString *user_Location;


-(void)setUserData:(NSDictionary*)userInfo;

@end
