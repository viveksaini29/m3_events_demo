//
//  M3UserInfo.m
//  M3
//
//  Created by Wegile on 13/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3UserInfo.h"

@implementation M3UserInfo

// Singleton instance

+ (id)shared {
    static M3UserInfo *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)setUserData:(NSDictionary*)userInfo {
    
    if (userInfo.count > 0) {
        
        NSDictionary *user_Info = userInfo[@"data"];
        
        if (user_Info[@"firstName"] != nil) {
            _user_FirstName = user_Info[@"firstName"];
            [[NSUserDefaults standardUserDefaults] setObject:_user_FirstName forKey:@"userFirstName"];
        }
        
        if (user_Info[@"lastName"] != nil) {
            _user_LastName = user_Info[@"lastName"];
            [[NSUserDefaults standardUserDefaults] setObject:_user_LastName forKey:@"userLastName"];
        }
        
        if (user_Info[@"email"] != nil) {
            _user_Email = user_Info[@"email"];
            [[NSUserDefaults standardUserDefaults] setObject:_user_Email forKey:@"userEmail"];
        }
        
        if (user_Info[@"profileImage"] != nil) {
            _user_Image = user_Info[@"profileImage"];
            [[NSUserDefaults standardUserDefaults] setObject:_user_Image forKey:@"userImage"];
        }
        
        if (user_Info[@"user_id"] != nil) {
            _user_Id = user_Info[@"user_id"];
            [[NSUserDefaults standardUserDefaults] setObject:_user_Id forKey:@"user_ID"];
        }
        
        if (user_Info[@"device_id"] != nil) {
            _device_Id = user_Info[@"device_id"];
            [[NSUserDefaults standardUserDefaults] setObject:_device_Id forKey:@"device_ID"];
        }
        
        if (user_Info[@"app_id"] != nil) {
            _app_Id = user_Info[@"app_id"];
            [[NSUserDefaults standardUserDefaults] setObject:_app_Id forKey:@"app_ID"];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end
