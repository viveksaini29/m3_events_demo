//
//  M3APIManager.h
//  M3SampleApp
//
//  Created by Wegile on 06/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M3APIManager : NSObject

@property(nonatomic,strong) NSString *imageURL;

+(id)sharedManager;
-(void)postRequestForSignUp:(NSString*)url imageType:(NSDictionary*)types param:(NSDictionary *)params :(void (^)(NSDictionary *data,int statusCode))success :(void (^)(NSError *error))failure;
-(void)getRequestWithURL:(NSString*)url :(void (^)(NSDictionary *data))completion;
-(void)postRequest:(NSString*)url param:(NSDictionary *)params :(void (^)(NSDictionary *data,int statusCode))success :(void (^)(NSError *error))failure;

@end
