//
//  M3APIManager.m
//  M3SampleApp
//
//  Created by Wegile on 06/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3APIManager.h"

@implementation M3APIManager

// Singleton instance
+ (id)sharedManager {
    static M3APIManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

/* Use this method for POST Request */

-(void)postRequestForSignUp:(NSString*)url imageType:(NSDictionary*)types param:(NSDictionary *)params :(void (^)(NSDictionary *data,int statusCode))success :(void (^)(NSError *error))failure {
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    NSString *urls = [NSString stringWithFormat:@"%s%@",baseUrl,url];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:urls parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (params[@"profileImage"] != nil) {
            
            NSString *url = [NSString stringWithFormat:@"%@",types[@"imageURL"]];
            if ([url length] != 0) {
                
                NSData *pictureData = [NSData dataWithContentsOfURL:types[@"imageURL"]];
                NSString *mimeType = [[M3Globals shared] mimeTypeForData:pictureData];
                NSString *tempFileName = [NSString stringWithFormat:@"image.%@",types[@"extension"]];
                
                [formData appendPartWithFileData:pictureData name:@"profileImage" fileName:tempFileName mimeType:mimeType];
                
            } else if ([url length] == 0 && ![types[@"extension"] isEqualToString:@""]) {
                NSData *pictureData = types[@"imageData"];
                NSString *mimeType = [[M3Globals shared] mimeTypeForData:pictureData];
                NSString *tempFileName = [NSString stringWithFormat:@"image.%@",types[@"extension"]];
                
                [formData appendPartWithFileData:pictureData name:@"profileImage" fileName:tempFileName mimeType:mimeType];
                
            }
        }
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //                          [progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          failure(error);
                          NSLog(@"Error: %@", error);
                      } else {
                          success(responseObject,[responseObject[@"code"] intValue]);
                          NSLog(@"%@ %@", response, responseObject);
                      }
                      [SVProgressHUD dismiss];
                  }];
    
    [uploadTask resume];
}

/* Use this method for GET Request */

-(void)getRequestWithURL:(NSString*)url :(void (^)(NSDictionary *data))completion {
    NSString *urls = [NSString stringWithFormat:@"%s%@",baseUrl,url];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urls parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        completion(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        
    }];
}

/* Use this method to verify OTP  */

-(void)postRequest:(NSString*)url param:(NSDictionary *)params :(void (^)(NSDictionary *data,int statusCode))success :(void (^)(NSError *error))failure {
    
    BOOL hasInternetConnection = [[Reachability reachabilityForInternetConnection] isReachable];
    // your code
    if (!hasInternetConnection) {
        [[AppDelegate M3AppDelegate] showAlert:@"Please check your internet connection."];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    NSString *urls = [NSString stringWithFormat:@"%s%@",baseUrl,url];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:urls parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"Response : %@", responseObject);
        
        [SVProgressHUD dismiss];
        success(responseObject,[responseObject[@"code"] intValue]);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
        failure(error);
        
    }];
    
}

@end
