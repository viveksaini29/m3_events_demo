//
//  AppDelegate.h
//  M3SampleApp
//
//  Created by Batman on 12/25/17.
//  Copyright © 2017 Batman. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <M3Sdk/M3Sdk.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+(AppDelegate*)M3AppDelegate;
-(void)pushViewController:(NSString*)identifier :(UINavigationController*)navController;
-(void)setMainRootViewController:(NSString*)indentifier;
-(void)setRootViewController:(NSString*)identifier;
-(void)showAlert:(NSString*)message;
- (NSString*)getValueFromDefaults:(NSString*)key;

@property(nonatomic,strong) NSMutableArray *arr_CartsList;
@property(nonatomic,strong) UINavigationController *myNav;

@property(nonatomic,strong) NSString *device_Id;
@property(nonatomic,strong) NSString *app_Id;
@end

