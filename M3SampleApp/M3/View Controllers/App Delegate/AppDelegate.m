//
//  AppDelegate.m
//  M3SampleApp
//
//  Created by Batman on 12/25/17.
//  Copyright © 2017 Batman. All rights reserved.
//

#import "AppDelegate.h"
//#import <M3Sdk/M3Sdk.h>
#import <M3EventsSdk/M3EventsSdk.h>
#import <M3EventsSdk/M3APIs.h>

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate () {
    int countOfApi;
}

@end

@implementation AppDelegate

+ (AppDelegate*)M3AppDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    _arr_CartsList = [[NSMutableArray alloc]init];
    [Fabric with:@[[Crashlytics class]]];
    //    // [M3Sdk initialize:@"tf2KyoyVajexC4BN7wCR" apiSecret:@"KhxDWzq6N0LvA64wvFr4"];
    //    [M3Sdk initialize:@"fwGXrgdoC0lBqp98UVZ2" apiSecret:@"cYJQTmqMZgJjUM1MOuC4"];
    //    //    [M3Sdk initialize:@"UGbtNYEmP1LJypUPo6rT" apiSecret:@"e6h7rHVGxgsera7py1kO"];
    //
    //    [M3Sdk push].notificationOptions = (M3NotificationOptionAlert | M3NotificationOptionBadge | M3NotificationOptionSound);
    //    [M3Sdk push].pushDelegate = self;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"success"] != nil) {
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"success"] == 1) {
            [self setMainRootViewController:@"M3HomePageVC"];
        } else {
            [self setRootViewController:@"M3WelcomeVC"];
        }
    } else {
        [self setRootViewController:@"M3WelcomeVC"];
    }
    
    //    NSLog(@"%@",[[M3Sdk user] userName]);
    //    NSLog(@"%@",[[M3Sdk user] userEmail]);
    
    //sasa.simovic931ca@gmail.com
    
    //    self.device_Id = [[M3Sdk push] deviceToken];
    //    self.app_Id = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    
    return YES;
}

//-(void)getLoginData {
//
//    BOOL success = [[NSUserDefaults standardUserDefaults]boolForKey:@"success"];
//    if (success == YES) {
//        [[NSUserDefaults standardUserDefaults]setValue:[[[M3Sdk user]preference] objectForKey:@"useremail"] forKey:@"useremail"];
//        [[NSUserDefaults standardUserDefaults]setValue:[[[M3Sdk user]preference] objectForKey:@"username"] forKey:@"username"];
//        [self setMainRootViewController:@"M3HomePageVC"];
//    } else {
//        NSLog(@"failed from sdk show alert here failed");
//
//        if (countOfApi > 2) {
//            [self showAlert:@"Something went wrong"];
//            return;
//        }
//        [self getLoginData];
//        countOfApi = countOfApi + 1;
//    }
//    [SVProgressHUD dismiss];
//}

- (void)showAlert:(NSString*)message {
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message"
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    // call method whatever u need
                                }];
    
    [alert addAction:yesButton];
    [self.window.rootViewController presentViewController:alert animated:YES completion:^{ }];
}

- (void)setMainRootViewController:(NSString*)indentifier {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:indentifier]]];
    
    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:6];
    mainViewController.leftViewEnabled = YES;
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
}

- (void)setRootViewController:(NSString*)identifier {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    UIWindow *window = UIApplication.sharedApplication.delegate.window;
    window.rootViewController = navigationController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
}

- (void)pushViewController:(NSString*)identifier :(UINavigationController*)navController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
    [navController pushViewController:vc animated:true];
}

- (void)passNotificationData:(NSDictionary *)dict isOpen:(BOOL)OnForeground {
    if (OnForeground==YES) {
        [self showMessageNotificationViewWithMessageObj:dict];
    } else {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"success"] != nil) {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"success"] == 1) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                M3DetailVC *detail = [storyboard instantiateViewControllerWithIdentifier:@"M3DetailVC"];
                detail.image= [UIImage imageNamed:@"movieBanner"];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:detail];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
            } else {
                [self setRootViewController:@"M3WelcomeVC"];
            }
        } else {
            [self setRootViewController:@"M3WelcomeVC"];
        }
        //        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_myNav animated:YES completion:nil];
    }
}

- (void)showMessageNotificationViewWithMessageObj:(NSDictionary*)data {
    
    NSDictionary *dict = data;
    NotificationChatView *topNotificationView = [[NotificationChatView alloc]initWithFrame:CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, 64)];
    topNotificationView.backgroundColor = [UIColor greenColor];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapChatNotfiicationView:)];
    recognizer.numberOfTapsRequired  = 1;
    [topNotificationView addGestureRecognizer:recognizer];
    topNotificationView.chatInfoDict = dict;
    
    UILabel *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 27, 290, 30)];
    messageLabel.textColor = [UIColor whiteColor] ;
    messageLabel.text = dict[@"aps"][@"alert"][@"title"];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.font = [UIFont systemFontOfSize:16];
    [topNotificationView addSubview:messageLabel];
    [topNotificationView removeFromSuperview];
    [self.window addSubview:topNotificationView];
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         topNotificationView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
                     }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:1.0 delay:1.5 options:UIViewAnimationOptionAllowUserInteraction
                          animations:^{
                          }
                          completion:^(BOOL finished) {
                              [self performSelector:@selector(removeNotificationView:) withObject:topNotificationView afterDelay:2.5];
                          }];
     }];
    
}

-(void)removeNotificationView:(NotificationChatView *)view {
    
    [UIView animateWithDuration:0.4 animations:^{
        for (UILabel *lblMessage in view.subviews) {
            lblMessage.alpha = 0.0;
        }
        view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
    
}

-(void)onTapChatNotfiicationView:(UITapGestureRecognizer *)recognizer {
    NotificationChatView *chatView = (NotificationChatView *)recognizer.view;
    [chatView removeFromSuperview];
    
    NSDictionary *dict = chatView.chatInfoDict;
    
    if ([dict valueForKey:@"aps"] != nil) {
        
        //        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_myNav animated:YES completion:nil];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"success"] != nil) {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"success"] == 1) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                M3DetailVC *detail = [storyboard instantiateViewControllerWithIdentifier:@"M3DetailVC"];
                detail.image= [UIImage imageNamed:@"movieBanner"];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:detail];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
            } else {
                [self setRootViewController:@"M3WelcomeVC"];
            }
        } else {
            [self setRootViewController:@"M3WelcomeVC"];
        }
    }
}

- (NSString*)getValueFromDefaults:(NSString*)key {
    
    NSString *value = @"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key] != nil) {
        
        value = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    } else {
        value = @""; // User default is nil
    }
    return value;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
