//
//  M3DetailVC.h
//  M3SampleApp
//
//  Created by Wegile on 27/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "M3Products.h"

@interface M3DetailVC : UIViewController
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,assign) NSInteger *myIndex;
@property(nonatomic,strong) M3Products *products;


@end
