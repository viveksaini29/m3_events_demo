//
//  M3DetailVC.m
//  M3SampleApp
//
//  Created by Wegile on 27/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3DetailVC.h"
#import <M3EventsSdk/M3APIs.h>

@interface M3DetailVC ()
@property (weak, nonatomic) IBOutlet UIImageView *img_Product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ProductName;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UIButton *btn_Like;

@end

@implementation M3DetailVC {
    NSMutableArray *arr_carts;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _lbl_ProductName.text = self.products.productName;
    _lbl_Price.text = [NSString stringWithFormat:@"%@",self.products.productPrice];
    _img_Product.image = _image;
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    mainViewController.leftViewEnabled=NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonActions:(UIButton *)sender {
    
    if (sender.tag==0) {
        [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
    } else if (sender.tag==1) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message"
                                                                       message:@"Add this product to Cart?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *OK = [UIAlertAction actionWithTitle:@"Ok"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 /** What we write here???????? **/
                                 [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
                                 
                                 arr_carts = [[NSMutableArray alloc]init];
                                 
                                 if ([[NSUserDefaults standardUserDefaults] objectForKey:@"carts_List"] != nil) {
                                     arr_carts = [[[NSUserDefaults standardUserDefaults] objectForKey:@"carts_List"] mutableCopy];
                                 }
                                 
                                 if (![arr_carts containsObject:[NSString stringWithFormat:@"%d",_myIndex]]) {
                                     [arr_carts addObject:[NSString stringWithFormat:@"%d",_myIndex]];
                                 }
                                 
                                 [[NSUserDefaults standardUserDefaults] setObject:arr_carts forKey:@"carts_List"];
                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                 
                                 [self addToCart];
                                 
                                 // call method whatever u need
                             }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:OK];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:^{ }];
        
    } else {
        if (sender.isSelected == true) {
            [sender setSelected:NO];
            [self setFavorite:NO];
            
            [sender setImage:[UIImage imageNamed:@"Dislike"] forState:UIControlStateNormal];
        } else {
            [sender setSelected:YES];
            [self setFavorite:true];
            [sender setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - JSON

- (void)setFavorite:(BOOL)isSelected {
    NSString *user_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"user_ID"];
    
    NSDictionary *params = @{@"device_id":[[AppDelegate M3AppDelegate] device_Id],@"application_id":[[AppDelegate M3AppDelegate] app_Id],@"user_id":user_Id,@"product_id":self.products.product_Id,@"is_like":@(isSelected)};

    [[M3APIs sharedAPI] postRequest:[NSString stringWithFormat:@"product_like/:%@",user_Id] param:params :^(NSDictionary *data, int statusCode) {
        NSLog(@"%@", data);
        [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
    } :^(NSError *error) {
        NSLog(@"%@", error);
    }];
}

-(void)addToCart {
    
    NSString *user_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"user_ID"];
    
    NSDictionary *params = @{@"device_id":[[AppDelegate M3AppDelegate] device_Id],@"application_id":[[AppDelegate M3AppDelegate] app_Id],@"user_id":user_Id,@"product_id":self.products.product_Id,@"quantity":@1};
    
    [[M3APIs sharedAPI] postRequest:[NSString stringWithFormat:@"add_to_cart/:%@",user_Id] param:params :^(NSDictionary *data, int statusCode) {
        
    } :^(NSError *error) {
        
    }];
}

@end
