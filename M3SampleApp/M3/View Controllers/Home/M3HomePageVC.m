
//
//  M3HomePageVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3HomePageVC.h"
#import <M3EventsSdk/M3APIs.h>

@interface M3HomePageVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbl_Products;

@end

@implementation M3HomePageVC
{
    NSArray *arr_Images;
    NSDictionary *finalData;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arr_Images = [[NSArray alloc]initWithObjects:@"icon_Product1",@"icon_Product2", nil];
    self.tbl_Products.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getAllProducts];
}

- (IBAction)buttonAction:(nullable id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:YES completionHandler:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- JSON

- (void) getAllProducts {
    
    [[M3APIs sharedAPI] getRequestWithURL:@"get_product_list" :^(NSDictionary *data, int statusCode) {
        NSLog(@"%@", data);
        if (data.count > 0) {
            if (statusCode == 200) {
                finalData = [[NSDictionary alloc]init];
                finalData=data;
                
                _tbl_Products.delegate = self;
                _tbl_Products.dataSource = self;
                [_tbl_Products reloadData];
                
            } else {
                [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
            }
        }
    }];
}

-(void)getProductDetail:(NSString*)product_Id {
    
    [[M3APIs sharedAPI] postRequest:@"product_detail" param:@{@"product_id":product_Id} :^(NSDictionary *data, int statusCode) {
        
    } :^(NSError *error) {
        
    }];
    
}

#pragma mark - UITableview Delegates and Data sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [finalData[@"data"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    M3ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (cell==nil) {
        cell = [[M3ProductCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSDictionary *allData = [finalData[@"data"] objectAtIndex:indexPath.row];
    [cell getAllProductData:allData path:finalData[@"product_image_url"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    M3ProductCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    M3DetailVC *vc = [story instantiateViewControllerWithIdentifier:@"M3DetailVC"];
    
    vc.products = [[M3Products alloc]init];
    [vc.products setProductInfo:[finalData[@"data"] objectAtIndex:indexPath.row]];
    vc.image = cell.img_Product.image;
    vc.myIndex = indexPath.row;
    
    NSDictionary *dict = [finalData[@"data"] objectAtIndex:indexPath.row];
    [self getProductDetail:dict[@"_id"]];
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
