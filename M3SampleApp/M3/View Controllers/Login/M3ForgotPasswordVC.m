//
//  M3ForgotPasswordVC.m
//  M3SampleApp
//
//  Created by Wegile on 27/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3ForgotPasswordVC.h"

@interface M3ForgotPasswordVC ()
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@end

@implementation M3ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txtEmail.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtEmail.layer.borderWidth  = 1.0;
    
    self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (IBAction)buttonActions:(UIButton *)sender {
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 
*/

@end
