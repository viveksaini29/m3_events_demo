//
//  M3LoginVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3LoginVC.h"

@interface M3LoginVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

@implementation M3LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txtEmail.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtEmail.layer.borderWidth  = 1.0;
    
    self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    self.txtPassword.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtPassword.layer.borderWidth  = 1.0;
    
    self.txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAlert:(NSString*)message {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message"
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    //                                    [self.navigationController popViewControllerAnimated:YES];
                                    // call method whatever u need
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:^{ }];
}

- (IBAction)buttonActions:(UIButton *)sender {
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if (sender.tag==1) {
        
        BOOL hasInternetConnection = [[Reachability reachabilityForInternetConnection] isReachable];
        // your code
        if (!hasInternetConnection) {
            [self showAlert:@"Please check your internet connection."];
            return;
        }
        
        else if ([_txtEmail.text isEqualToString:@""]) {
            [self showAlert:@"Please enter email."];
        } else if ([_txtPassword.text isEqualToString:@""]) {
            [self showAlert:@"Please enter password."];
        } else if ([[M3Globals shared] validEmail:_txtEmail.text] == NO) {
            [self showAlert:@"Please enter valid email."];
        } else {
            
            NSDictionary *params = @{@"email":_txtEmail.text,@"password":_txtPassword.text,@"device_id":@"xyz",@"application_id":@"12345"};
            
                      [[M3APIManager sharedManager] postRequest:@"sign_in" param:params :^(NSDictionary *data, int statusCode) {
             if (data.count > 0) {
             if (statusCode == 200) {
             [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
             [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"success"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             [[M3UserInfo shared] setUserData:data];
             } else {
             [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
             }
             }
             }:^(NSError *error) {
             [[AppDelegate M3AppDelegate] showAlert:error.description];
             }];
             
            
            
    /*        [SVProgressHUD showWithStatus:@"Loading..."];
            [[AppDelegate M3AppDelegate] signUpuser:_txtEmail.text username:@"Sam"];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Sam" forKey:@"userFirstName"];
            [[NSUserDefaults standardUserDefaults] setObject:_txtEmail.text forKey:@"userEmail"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            */
        }
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
