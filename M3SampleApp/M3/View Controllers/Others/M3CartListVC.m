
//
//  M3CartListVC.m
//  M3SampleApp
//
//  Created by Wegile on 01/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3CartListVC.h"

@interface M3CartListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbl_Products;

@end

@implementation M3CartListVC
{
    NSArray *arr_Images;
    NSMutableArray *arr_crts;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arr_Images = [[NSArray alloc]initWithObjects:@"icon_Product1",@"icon_Product2", nil];
    self.tbl_Products.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"carts_List"] != nil) {
        arr_crts = [[NSMutableArray alloc]init];
        arr_crts = [[[NSUserDefaults standardUserDefaults] objectForKey:@"carts_List"] mutableCopy];
        NSLog(@"Datas%@",arr_crts);
    }
}

- (IBAction)buttonAction:(nullable id)sender {
    [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableview Delegates and Data sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections;
    if (arr_crts.count) {
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        sections = 1;
        tableView.backgroundView = nil;
    } else  {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No carts available";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        sections = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return sections;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arr_crts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    M3CartsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (cell==nil) {
        cell = [[M3CartsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (arr_crts.count >= 2) {
        if ([arr_crts containsObject:[NSString stringWithFormat:@"%d",0]]) {
            if (indexPath.row == 0) {
                cell.img_Product.image = [UIImage imageNamed:[arr_Images objectAtIndex:0]];
            }
        }
        
        if ([arr_crts containsObject:[NSString stringWithFormat:@"%d",1]]) {
            if (indexPath.row == 1) {
                cell.img_Product.image = [UIImage imageNamed:[arr_Images objectAtIndex:1]];
            } else {
                cell.img_Product.image = [UIImage imageNamed:[arr_Images objectAtIndex:0]];
                
            }
        }
    } else if (arr_crts.count <= 1) {
        if ([arr_crts containsObject:[NSString stringWithFormat:@"%d",0]]) {
            if (indexPath.row == 0) {
                cell.img_Product.image = [UIImage imageNamed:[arr_Images objectAtIndex:0]];
            }
        }
        
        if ([arr_crts containsObject:[NSString stringWithFormat:@"%d",1]]) {
            if (indexPath.row == 0) {
                cell.img_Product.image = [UIImage imageNamed:[arr_Images objectAtIndex:1]];
            }
        }
    }
    
    
    cell.btn_RemoveItem.tag = indexPath.row;
    [cell.btn_RemoveItem addTarget:self action:@selector(removeItem:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(void)removeItem:(UIButton*)sender {
    int index = sender.tag;
    [arr_crts removeObjectAtIndex:index];
    
    [[NSUserDefaults standardUserDefaults] setObject:arr_crts forKey:@"carts_List"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [_tbl_Products beginUpdates];
    [_tbl_Products deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    [_tbl_Products endUpdates];
    [_tbl_Products reloadData];
}

- (void)getMyCarts {
    
//    [M3APIs sharedAPI] postRequest:@"" param:<#(NSDictionary *)#> :<#^(NSDictionary *data, int statusCode)success#> :<#^(NSError *error)failure#>
}
@end
