//
//  M3MyProfileVC.m
//  M3SampleApp
//
//  Created by Wegile on 01/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3MyProfileVC.h"
#import "UIImageView+AFNetworking.h"

@interface M3MyProfileVC ()
@property (weak, nonatomic) IBOutlet UIImageView *img_User;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Email;

@end

@implementation M3MyProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSURL *url = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"userImage"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:data];
        _img_User.image= img;
    });
    
    _lbl_Name.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userFirstName"];
    _lbl_Email.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonActions:(UIButton *)sender {
    switch (sender.tag) {
        case 0:{
            [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
            break;
        }
        case 4: {
            
            NSString *user_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"user_ID"];
            NSString *app_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"app_ID"];
            NSString *device_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"device_ID"];
            
            NSDictionary *params= @{@"device_id":device_Id,@"application_id":app_Id,@"user_id":user_Id};
            [[M3APIManager sharedManager] postRequest:@"sign_out" param:params :^(NSDictionary *data, int statusCode) {
                if (data.count > 0) {
                    if (statusCode == 200) {
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"success"];
                        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"useremail"];
                        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
                        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"success"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[AppDelegate M3AppDelegate] setRootViewController:@"M3WelcomeVC"];
                    } else {
                        [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
                    }
                }
            }:^(NSError *error) {
                [[AppDelegate M3AppDelegate] showAlert:error.description];
            }];
            break;
            
            
            //            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"success"];
            //            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"useremail"];
            //            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
            //            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"success"];
            //            [[NSUserDefaults standardUserDefaults] synchronize];
            //            [[AppDelegate M3AppDelegate] setRootViewController:@"M3WelcomeVC"];
            
        }
        default:
            break;
    }
}

@end
