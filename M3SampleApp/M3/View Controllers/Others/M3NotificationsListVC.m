//
//  M3NotificationsListVC.m
//  M3SampleApp
//
//  Created by Wegile on 01/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3NotificationsListVC.h"

@interface M3NotificationsListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbl_Notifications;

@end

@implementation M3NotificationsListVC
{
    NSArray *arr_Images;
    NSArray *arr_Titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arr_Images = [[NSArray alloc]initWithObjects:@"",@"", nil];
    arr_Titles = [[NSArray alloc]initWithObjects:@"",@"", nil];
}

- (IBAction)buttonActions:(UIButton *)sender {
    [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UITableView Delegates and Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arr_Images.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    M3NotificationsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (cell==nil) {
        cell = [[M3NotificationsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.img_Item.image = [UIImage imageNamed:@"icon_Product1"];
    cell.lbl_Title.text = @"hiii";
    cell.lbl_Description.text = @"Descpin";
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
@end
