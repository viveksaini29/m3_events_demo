//
//  M3SignUpVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3SignUpVC.h"

@interface M3SignUpVC ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrolView;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *img_User;

@end

@implementation M3SignUpVC
{
    NSString *imageURLFromPicker;
    NSString *imageExtension;
    BOOL img_Selected;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.txtFirstName.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtFirstName.layer.borderWidth  = 1.0;
    
    self.txtLastName.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtLastName.layer.borderWidth  = 1.0;
    
    self.txtEmail.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtEmail.layer.borderWidth  = 1.0;
    
    self.txtPassword.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtPassword.layer.borderWidth  = 1.0;
    
    self.txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    _img_User.layer.cornerRadius = _img_User.frame.size.width/2;
    _img_User.layer.masksToBounds=YES;
    
}

- (IBAction)selectImage:(UIButton *)sender {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Select image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From Library",@"From Camera", nil];
    
    [action showInView:self.view];
}

#pragma mark - ActionSheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 ) {
        
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate = self;
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:pickerView animated:YES completion:nil];
        
    } else if( buttonIndex == 1 ) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *pickerView =[[UIImagePickerController alloc]init];
            pickerView.allowsEditing = YES;
            pickerView.delegate = self;
            pickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pickerView animated:YES completion:nil];
        }
    }
}

#pragma mark - PickerDelegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    
    _img_User.image = img;
    img_Selected=YES;
    
    NSURL *aset = info[UIImagePickerControllerReferenceURL];
    NSLog(@"%@",aset);
    if ([[aset absoluteString] hasSuffix:@"JPG"]) {
        imageExtension = @"jpg";
        
        NSLog(@"JPG");
    } else if ([[aset absoluteString] hasSuffix:@"PNG"]) {
        NSLog(@"PNG");
        imageExtension = @"png";
        
    } else if ([[aset absoluteString] hasSuffix:@"GIF"]) {
        NSLog(@"GIF");
        imageExtension = @"gif";
        
    } else {
        NSLog(@"NO EXTENTION");
    }
    
    if (@available(iOS 11.0, *)) {
        imageURLFromPicker = info[UIImagePickerControllerImageURL];
    } else {
        // Fallback on earlier versions
        imageURLFromPicker = @"";
    }
    
    if (aset == nil) {
        //        UIImage *image = info[UIImagePickerControllerOriginalImage];
        ////        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        //
        //        NSData *data=UIImagePNGRepresentation(image);
        //
        //        NSString *FileName=[NSString stringWithFormat:@"test1.png"];
        //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //        NSString *documentsDirectory = [paths objectAtIndex:0];
        //        NSString *tempPath = [documentsDirectory stringByAppendingPathComponent:FileName];
        //        [data writeToFile:tempPath atomically:YES];
        
        imageExtension = @"png";
        imageURLFromPicker = @"";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonActions:(UIButton *)sender {
    
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if (sender.tag==1) {
        
        BOOL hasInternetConnection = [[Reachability reachabilityForInternetConnection] isReachable];
        // your code
        if (!hasInternetConnection) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please check your internet connection."];
            return;
        }
        else if ([_txtFirstName.text isEqualToString:@""]) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please enter firstname."];
        } else if ([_txtLastName.text isEqualToString:@""]) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please enter lastname."];
        } else if ([_txtEmail.text isEqualToString:@""]) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please enter email."];
        } else if ([_txtPassword.text isEqualToString:@""]) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please enter password."];
        } else if ([[M3Globals shared] validEmail:_txtEmail.text] == NO) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please enter valid email."];
        } else {
            
                    if (!img_Selected) {
             imageURLFromPicker = @"";
             imageExtension = @"";
             }
             
             NSDictionary *params = @{@"firstName":_txtFirstName.text,@"lastName":_txtLastName.text,@"address":@"12345",@"email":_txtEmail.text,@"password":_txtPassword.text,@"profileImage":imageExtension,@"device_id":@"xyz",@"contactNumber":@"12345",@"verify_method":@"email",@"app_id":@"12345",@"location":@"xyz"};
             
             NSData *data=UIImagePNGRepresentation(_img_User.image);
             
             NSDictionary *imageTypes = @{@"extension":imageExtension,@"imageURL":imageURLFromPicker,@"imageData":data};
             
             [[M3APIManager sharedManager] postRequestForSignUp:@"create_profile" imageType:imageTypes param:params :^(NSDictionary *data, int statusCode) {
             if (data.count > 0) {
             if (statusCode == 200) {
             
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             M3VerifyOTPVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"M3VerifyOTPVC"];
             vc.verifyEmail = _txtEmail.text;
             [self.navigationController pushViewController:vc animated:true];
             } else {
             [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
             }
             }
             } :^(NSError *error) {
             [[AppDelegate M3AppDelegate] showAlert:error.description];
             }];
            
            
          //  [SVProgressHUD showWithStatus:@"Loading..."];
//            [[AppDelegate M3AppDelegate]signUpuser:_txtEmail.text username:_txtFirstName.text];
            
//            [[NSUserDefaults standardUserDefaults] setObject:_txtFirstName.text forKey:@"userFirstName"];
//            [[NSUserDefaults standardUserDefaults] setObject:_txtEmail.text forKey:@"userEmail"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
