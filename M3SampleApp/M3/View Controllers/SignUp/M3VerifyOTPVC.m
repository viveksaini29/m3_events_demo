//
//  M3VerifyOTPVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3VerifyOTPVC.h"

@interface M3VerifyOTPVC ()<UITextFieldDelegate,ActivationCodeTextFieldDelegate>
@property (weak, nonatomic) IBOutlet ActivationCodeTextField *txtOTP;

@end

@implementation M3VerifyOTPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    self.txtOTP.layer.borderColor = [[UIColor orangeColor] CGColor];
    //    self.txtOTP.layer.borderWidth  = 1.0;
    
    self.txtOTP.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
    self.txtOTP.maxCodeLength = 4;
    self.txtOTP.customPlaceholder = @"-";
    self.txtOTP.keyboardType = UIKeyboardTypeDefault;
    self.txtOTP.borderStyle = UITextBorderStyleNone;
    self.txtOTP.tag = 1;
    self.txtOTP.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtOTP.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtOTP.delegate = self;
    self.txtOTP.activationCodeTFDelegate = self;
    self.txtOTP.textAlignment = NSTextAlignmentCenter;
    self.txtOTP.textColor = [UIColor blackColor];
    self.txtOTP.font = [UIFont fontWithName:@"SEGOEUI" size:18];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonActions:(UIButton *)sender {
    
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        if ([_txtOTP.text isEqualToString:@""]) {
            [[AppDelegate M3AppDelegate] showAlert:@"Please enter OTP."];
            return;
        }
        NSDictionary *params= @{@"verify_account":_txtOTP.text,@"email":_verifyEmail};
        [[M3APIManager sharedManager] postRequest:@"verify_account" param:params :^(NSDictionary *data, int statusCode) {
            if (data.count > 0) {
                if (statusCode == 200) {
                    
//                    [[M3Sdk user] setUserName:data[@"data"][@"firstName"]];
//                    [[M3Sdk user] setUserEmail:data[@"data"][@"email"]];
                    
                    [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
                    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"success"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                } else {
                    [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
                }
            }
        }:^(NSError *error) {
            [[AppDelegate M3AppDelegate] showAlert:error.description];
        }];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
