//
//  M3VerifyVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3VerifyVC.h"

@interface M3VerifyVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@end

@implementation M3VerifyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txtPhoneNumber.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.txtPhoneNumber.layer.borderWidth  = 1.0;
    
    self.txtPhoneNumber.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonActions:(UIButton *)sender {
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
