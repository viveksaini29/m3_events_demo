//
//  M3NearByVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3NearByVC.h"

@interface M3NearByVC ()

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;

@property (weak, nonatomic) IBOutlet UIButton *btnNearBy;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCity;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControlIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewExplainer;

@end

@implementation M3NearByVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.btnSelectCity.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnSelectCity.layer.borderWidth  = 1.0;
    
    CGRect frame = CGRectMake(0, 0, 0, 0);
    
    for (int index=0;index<3;index++)
    {
        frame.origin.x = _scrollViewExplainer.frame.size.width * index;
        frame.origin.y = _scrollViewExplainer.frame.origin.y;
        frame.size.width = _scrollViewExplainer.frame.size.width;
        frame.size.height = _scrollViewExplainer.frame.size.height;
        
        switch (index) {
            case 0:
                _view1.frame = frame;
                [_scrollViewExplainer addSubview:_view1];
                break;
            case 1:
                _view2.frame = frame;
                [_scrollViewExplainer addSubview:_view2];
                break;
                
            case 2:
                _view3.frame = frame;
                [_scrollViewExplainer addSubview:_view3];
                break;
                
            default:
                break;
        }
    }
    _scrollViewExplainer.contentSize = CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
     int pageCount = round(scrollView.contentOffset.x/self.view.frame.size.width);
    _pageControlIndicator.currentPage = pageCount;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonAction:(UIButton *)sender {
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else if (sender.tag==2) {
        [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];

        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"success"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
}

@end
