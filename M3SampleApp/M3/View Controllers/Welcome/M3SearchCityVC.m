//
//  M3SearchCityVC.m
//  M3SampleApp
//
//  Created by Wegile on 27/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3SearchCityVC.h"

@interface M3SearchCityVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITextField *txt_Search;
@property (nonatomic,weak) IBOutlet UIButton *btn_Cross;
@property (nonatomic,weak) IBOutlet UIImageView *img_Cross;
@property (nonatomic,weak) IBOutlet UITableView *tbl_Cities;

@end

@implementation M3SearchCityVC
{
    NSMutableArray *arr_All;
    NSMutableArray *arr_SearchData;
    NSString *str_Search;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [_tbl_Cities setHidden:YES];
    [_btn_Cross setHidden:YES];
    
    arr_All = [[NSMutableArray alloc]init];
    [arr_All addObject:@"Canada"];
    [arr_All addObject:@"Mumbai"];
    [arr_All addObject:@"America"];
    [arr_All addObject:@"NewYork"];
    [arr_All addObject:@"Barcelona"];
    
    [self.tbl_Cities registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tbl_Cities setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonActions:(UIButton *)sender {
    if (sender.tag==0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        
        [arr_SearchData removeAllObjects];
        [_tbl_Cities reloadData];
        [_tbl_Cities setHidden:YES];
        _txt_Search.text = @"";
        [self.view endEditing:YES];
        
    }
}

#pragma mark - UITableViewDelegtes and Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arr_SearchData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSString *str_Value = [arr_SearchData objectAtIndex:indexPath.row];
    cell.textLabel.text = str_Value;
    
    cell.detailTextLabel.text = str_Value;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.imageView.image = [UIImage imageNamed:@"location"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    [[AppDelegate M3AppDelegate] setMainRootViewController:@"M3HomePageVC"];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"success"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [SVProgressHUD dismiss];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UITextField Delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@""]) {
        //        str_Search = str_Search
    } else {
        str_Search = [NSString stringWithFormat:@"%@%@", _txt_Search.text,string];
        [_btn_Cross setHidden:NO];
        
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@",str_Search];
    NSArray *arr_New = [arr_All filteredArrayUsingPredicate:predicate];
    if (arr_New.count>0) {
        [arr_SearchData removeAllObjects];
        arr_SearchData = [arr_New mutableCopy];
        [_tbl_Cities setHidden:NO];
        
        
        CGRect frame= _tbl_Cities.frame;
        frame.size.height = arr_SearchData.count*60.0;
        _tbl_Cities.frame = frame;
        [_tbl_Cities reloadData];
    } else {
        [_tbl_Cities setHidden:YES];
    }
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
