//
//  M3WelcomeVC.m
//  M3SampleApp
//
//  Created by Wegile on 26/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3WelcomeVC.h"

@interface M3WelcomeVC ()
@property (weak, nonatomic) IBOutlet UIButton *btn_Guest;
@property (weak, nonatomic) IBOutlet UIButton *btn_SignUp;

@end

@implementation M3WelcomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.btn_Guest.layer.borderColor = [[UIColor blackColor] CGColor];
    self.btn_Guest.layer.borderWidth  = 1.0;
    
    self.btn_SignUp.layer.borderColor = [[UIColor blackColor] CGColor];
    self.btn_SignUp.layer.borderWidth  = 1.0;
    
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
