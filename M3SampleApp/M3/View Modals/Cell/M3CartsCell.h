//
//  M3CartsCell.h
//  M3SampleApp
//
//  Created by Wegile on 01/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface M3CartsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_Product;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView1;
@property (weak, nonatomic) IBOutlet UIButton *btn_RemoveItem;

@end
