//
//  M3CartsCell.m
//  M3SampleApp
//
//  Created by Wegile on 01/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3CartsCell.h"

@implementation M3CartsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _ratingView1.maximumValue = 5;
    _ratingView1.minimumValue = 0;
    _ratingView1.value = 4;
    //    _ratingView1.tintColor = [UIColor redColor];
    _ratingView1.allowsHalfStars = YES;
    //    _ratingView1.emptyStarImage = [[UIImage imageNamed:@"heart-empty"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingView1.filledStarImage = [[UIImage imageNamed:@"icon_Star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
