//
//  M3NotificationsCell.h
//  M3SampleApp
//
//  Created by Wegile on 01/03/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface M3NotificationsCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIImageView *img_Item;
@property (nonatomic,weak) IBOutlet UILabel *lbl_Title;
@property (nonatomic,weak) IBOutlet UILabel *lbl_Description;
@end
