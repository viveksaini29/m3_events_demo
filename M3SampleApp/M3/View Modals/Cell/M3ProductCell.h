//
//  M3ProductCell.h
//  M3SampleApp
//
//  Created by Wegile on 27/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface M3ProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_Product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ProductName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView1;


- (void)getAllProductData:(NSDictionary*)data path:(NSString*)imagePath;
@end
