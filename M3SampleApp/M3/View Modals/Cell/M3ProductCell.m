//
//  M3ProductCell.m
//  M3SampleApp
//
//  Created by Wegile on 27/02/18.
//  Copyright © 2018 Batman. All rights reserved.
//

#import "M3ProductCell.h"

@implementation M3ProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _ratingView1.maximumValue = 5;
    _ratingView1.minimumValue = 0;
    _ratingView1.value = 4;
    //    _ratingView1.tintColor = [UIColor redColor];
    _ratingView1.allowsHalfStars = YES;
    //    _ratingView1.emptyStarImage = [[UIImage imageNamed:@"heart-empty"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingView1.filledStarImage = [[UIImage imageNamed:@"icon_Star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
}

#pragma mark - Get JSON

- (void)getAllProductData:(NSDictionary*)data path:(NSString*)imagePath {
    
    if (data.count > 0) {
        NSString *image = [imagePath stringByAppendingString:data[@"productImage"]];
        self.lbl_ProductName.text = data[@"productName"];
        self.lbl_Price.text = [NSString stringWithFormat:@"%@",data[@"productPrice"]];
        NSLog(@"%@", image);
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSURL *url = [NSURL URLWithString:image];
            NSData *dataa = [NSData dataWithContentsOfURL:url];
            
            UIImage* image = [[UIImage alloc] initWithData:dataa];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _img_Product.image = image;
                    [self setNeedsLayout];
                });
            }
            
        });
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
