//
//  LeftViewController.m
//  LGSideMenuControllerDemo
//

#import "LeftViewController.h"

@interface LeftViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSArray *titlesArray;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Items;

@end

@implementation LeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // -----
    
    self.titlesArray = @[@"HOME/DISCOVER",
                         @"MY PROFILE",
                         @"MY CART",
                         @"NOTIFICATIONS",
                         @"SIGN IN/SIGN OUT"];
    
    // -----
    
}
- (IBAction)buttonActions:(UIButton *)sender {
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titlesArray.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LeftViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.titleLabel.text = self.titlesArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    if (indexPath.row == 0) {
        
        UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"M3HomePageVC"];
        [navigationController setViewControllers:@[viewController]];
        [mainViewController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
        
    } else if (indexPath.row == 1) {
        [self goToViewController:@"M3MyProfileVC" :YES];
        
    } else if (indexPath.row == 2) {
        [self goToViewController:@"M3CartListVC" :YES];
        
    } else if (indexPath.row == 3) {
        [self goToViewController:@"M3NotificationsListVC" :YES];
        
    } else if (indexPath.row == 4) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                      message:@"Do you want to logout?"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 NSString *user_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"user_ID"];
                                 NSString *app_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"app_ID"];
                                 NSString *device_Id = [[AppDelegate M3AppDelegate] getValueFromDefaults:@"device_ID"];
                                 
                                 NSDictionary *params= @{@"device_id":device_Id,@"application_id":app_Id,@"user_id":user_Id};
                                 [[M3APIManager sharedManager] postRequest:@"sign_out" param:params :^(NSDictionary *data, int statusCode) {
                                     if (data.count > 0) {
                                         if (statusCode == 200) {
                                             [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"success"];
                                             [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"useremail"];
                                             [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
                                             [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"success"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             [[AppDelegate M3AppDelegate] setRootViewController:@"M3WelcomeVC"];
                                         } else {
                                             [[AppDelegate M3AppDelegate] showAlert:data[@"message"]];
                                         }
                                     }
                                 }:^(NSError *error) {
                                     [[AppDelegate M3AppDelegate] showAlert:error.description];
                                 }];
              
                         /*        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"success"];
                                 [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"useremail"];
                                 [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
                                 [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"success"];
                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                 [[AppDelegate M3AppDelegate] setRootViewController:@"M3WelcomeVC"]; */
                                 /** What we write here???????? **/
                                 // call method whatever u need
                             }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
    }
    else {
        UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"M3HomePageVC"];
        [navigationController setViewControllers:@[viewController]];
        [mainViewController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
    }
}


- (void)goToViewController:(NSString*)identifier :(BOOL)gestureEnable {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [navigationController setViewControllers:@[viewController]];
    
    // Rarely you can get some visual bugs when you change view hierarchy and toggle side views in the same iteration
    // You can use delay to avoid this and probably other unexpected visual bugs
    [mainViewController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
    mainViewController.leftViewSwipeGestureDisabled=gestureEnable;
}

@end
