//
//  NotificationChatView.h
//  Skoopa
//
//  Created by Tarun Khosla on 28/04/16.
//  Copyright © 2016 Wegile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationChatView : UIView {
    NSDictionary *chatInfoDict;
}

@property (nonatomic, retain) NSDictionary *chatInfoDict;


@end
