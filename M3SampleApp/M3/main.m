//
//  main.m
//  M3SampleApp
//
//  Created by Batman on 12/25/17.
//  Copyright © 2017 Batman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
